package si.evil.messages;

/**
 * TODO: refactor to Target?
 * @author gcasar
 *
 */
public class DummyTarget implements Target{
	protected static int last_id = 0;
	
	/**
	 * Simple sequential implementation
	 * @param existing
	 * @return
	 */
	public static int generateNewUID(){
		return (++last_id);
	}
	
	protected int uid;
	
	public DummyTarget(int uid){
		this.uid = uid;
	}
	
	public int getUID(){return uid;}
	
	public void setUID(int uid){this.uid = uid;}
	
	/**
	 * Override to recv raw messages
	 * @param msg
	 */
	public boolean recvMessage(Target sender, Message msg){
		return false;
	}
}

