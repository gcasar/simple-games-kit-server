package si.evil.messages;

import si.evil.util.Serializer;

/**
 * Messages of this type are fixed length and can be buffered because of it
 * @author gcasar
 *
 */
public abstract class BufferedMessage extends Message {
	
	protected byte[] buffer = null;
	
	@Override
	public byte[] serialize(Serializer s) {
		if(buffer==null)
			s.useInternal();
		else s.wrap(buffer);
		int length = toBytes(s);
		if(length>0){
			byte[] result = new byte[length];
			byte[] source = s.getBytes();
			while(length-->0){//<--
				result[length] = source[length];
			}
			return buffer=result;
		}else return null;
	}

}
