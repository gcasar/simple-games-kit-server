package si.evil.messages;

import java.util.ArrayList;
import java.util.Collection;

import si.evil.server.Buffer;


/**
 * To be implemented by a deploy target: if a remote server than a socket like object,
 * if by local than may be a simple call to another method or even a thread messaging system.
 * @author gcasar
 *
 */
public abstract class MessageDispatcher {
	protected MessageParser mp;
	
	/**
	 * Registers a new target (in practice this may associate the target to a socket)
	 * @param t
	 * @return uid
	 */
	public abstract int register(Target t);
	
	/**
	 * Checks if the given target is associated with an UID
	 * @param t
	 * @return the associated uid or 0
	 */
	public abstract int uid(Target t);
	
	/**
	 * Sends to target
	 * @param p
	 * @param msg
	 */
	public abstract void sendMessage(Target p, Message msg);
	
	/**
	 * Sends to all
	 * @param msg
	 */
	public abstract void brodcastMessage(Message msg);
	
	/**
	 * Sends to targets
	 * @param targets
	 * @param msg
	 */
	public abstract void multicastMessage(Collection<Target> targets, Message msg);
	
	/**
	 * Send to all but ignored
	 * @param ignored
	 * @param msg
	 */
	public abstract void invMulticastMessage(Collection<Target> ignored, Message msg);
	
	/**
	 * Send to all but ignored
	 * @param ignored
	 * @param msg
	 */
	public abstract void invSendMessage(Target ignored, Message msg);
	
	public void setMessageParser(MessageParser mp){
		this.mp = mp;
	}
	
	public Message parse(Buffer b){
		return mp.parse(b);
	}
}
