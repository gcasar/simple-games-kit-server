package si.evil.messages;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import si.evil.simpleg.server.Player;


public class LoopbackMsgDispatcher extends MessageDispatcher{
	
	HashMap<Target, Integer> targets;
	
	protected Target id;
	
	public LoopbackMsgDispatcher(){
		targets = new HashMap<Target,Integer>();
	}
	
	public LoopbackMsgDispatcher(HashMap<Target,Integer> players, Target id_sender){
		this.targets = players;
		this.id = id_sender;
	}
	
	public void setSenderID(Target id){
		this.id = id;
	}

	/**
	 * Does not check if the target is valid
	 */
	@Override
	public void sendMessage(Target p, Message msg) {
		p.recvMessage(this.id, msg);
	}

	@Override
	public void brodcastMessage(Message msg) {
		for(Target t: targets.keySet()){
			t.recvMessage(this.id, msg);
		}
	}

	@Override
	public void multicastMessage(Collection<Target> targets, Message msg) {
		for(Target t: targets){
			t.recvMessage(this.id, msg);
		}
	}

	@Override
	public void invMulticastMessage(Collection<Target> ignored, Message msg) {
		for(Target t: targets.keySet()){
			if(ignored.contains(t))continue;
			t.recvMessage(this.id, msg);
		}
	}

	@Override
	public void invSendMessage(Target ignored, Message msg) {
		for(Target t: targets.keySet()){
			if(t==ignored)continue;
			t.recvMessage(this.id, msg);
		}
	}

	@Override
	public int register(Target t) {
		int tmp = Player.generateNewUID();
		targets.put(t, tmp);
		return tmp;
	}

	@Override
	public int uid(Target t) {
		// TODO Auto-generated method stub
		if(targets.containsKey(t))
			return targets.get(t);
		return 0;
	}


}
