package si.evil.messages;


import si.evil.util.Serializer;
import si.evil.util.UnderflowException;

public abstract class Message {
	public static final byte MSG_PLAYER = 0;
	
	public int _id;
	
	public static boolean checkId(byte[] raw, Serializer s, int id) {
		s.wrap(raw);
		try {
			return s.readInt()==id;
		} catch (UnderflowException e) {
			return false;
		}
	}

	public boolean parse(byte[] raw, Serializer s) {
		s.wrap(raw);
		return this.parse(s);
	}
	
	public abstract boolean parse(Serializer s);
	public abstract int toBytes(Serializer s);

	public byte[] serialize(Serializer s) {
		s.useInternal();
		int length = toBytes(s);
		if(length>0){
			byte[] result = new byte[length];
			byte[] source = s.getBytes();
			while(length-->0){//<--
				result[length] = source[length];
			}
			return result;
		}else return null;

	}
}
