package si.evil.messages;



/**
 * Target for MessageDispatcher
 * Contains an unique id
 * @author gcasar
 *
 */

public interface Target {
	
	public boolean recvMessage(Target sender, Message msg);
	
	public int getUID();
	
}
