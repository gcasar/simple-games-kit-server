package si.evil.messages;

import si.evil.server.Buffer;
import si.evil.util.GWTByteSerializer;
import si.evil.util.Serializer;

/**
 * Must not be using java reflection because of GWT compactibility. So we stick to overrides.<br>
 * Extend and override 
 * @author gcasar
 *
 */
public abstract class MessageParser {
	
	Serializer serializer;
	
	/**
	 * Same as MessageParser(new GWTByteSerializer());
	 */
	public MessageParser(){
		this.serializer = new GWTByteSerializer();
	}
	
	public MessageParser(Serializer s){
		this.serializer = s;
	}
	
	public abstract Message parse(Buffer b);
	
	
}
