
package si.evil.util;

import java.io.UnsupportedEncodingException;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

/**
 * Uses the same serialization as GWTByteSerializer. Might be a bit faster. Bad representation of floats.
 * @author gcasar
 *
 */
public class JavaByteSerializer implements Serializer{
	public static final float FLOAT_ACCURACY = 10000;
	/**
	 * in bytes
	 */
	public static final int BUFFER_SIZE = 5*1024;
	ByteBuffer bb = null;
	
	protected byte[] buffer = new byte[BUFFER_SIZE];

	@Override
	public void wrap(byte[] buffer) {
		bb = ByteBuffer.wrap(buffer);
	}
	

	@Override
	public void useInternal() {
		wrap(buffer);
	}

	/**
	 * Not correct (terrrrrible loss of accuracy), but sufficient. Does not give a shit about underlying float type
	 */
	@Override
	public float readFloat() throws UnderflowException {
		try{
			int raw = bb.getInt();
			return raw/FLOAT_ACCURACY;
		}catch(BufferUnderflowException e){
			throw new UnderflowException();
		}
	}

	@Override
	public int readInt() throws UnderflowException {
		try{
			return bb.getInt();
		}catch(BufferUnderflowException e){
			throw new UnderflowException();
		}
	}

	@Override
	public String readString() throws UnderflowException {
		String result;
		try{
			int len = bb.getInt();
			if(len>0){
				//Try to read the string
				if(len>bb.remaining())throw new UnderflowException();
				try {
					result = new String(bb.array(), bb.position(), len, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					System.out.println("Serializer: bad encoding.");
					throw new UnderflowException();
				}
				bb.position(bb.position()+len);
				return result;
			}else return "";
		}catch(BufferUnderflowException e){
			throw new UnderflowException();
		}
	}
	

	@Override
	public byte readByte() throws UnderflowException {
		try{
			return bb.get();
		}catch(BufferUnderflowException e){
			throw new UnderflowException();
		}
	}
	

	@Override
	public short readShort() throws UnderflowException {
		try{
			return bb.getShort();
		}catch(BufferUnderflowException e){
			throw new UnderflowException();
		}
	}

	@Override
	public void putShort(short i) throws OverflowException {
		try{
			bb.putShort(i);
		}catch(BufferOverflowException e){
			throw new OverflowException();
		}
	}

	@Override
	public void putFloat(float f) throws OverflowException {
		try{
			bb.putInt((int) (f*FLOAT_ACCURACY));
		}catch(BufferOverflowException e){
			throw new OverflowException();
		}
	}
	
	@Override
	public void putInt(int i) throws OverflowException {
		try{
			bb.putInt(i);
		}catch(BufferOverflowException e){
			throw new OverflowException();
		}
	}

	@Override
	public int putString(String str) throws OverflowException {
		try{
			byte[] raw = null;
			if(str!=null){
				try {
					raw = str.getBytes("UTF-8");
				} catch (UnsupportedEncodingException e) {
					System.out.println("Serializer: bad encoding (put string).");
					throw new OverflowException();
				}
				bb.putInt(raw.length);
				bb.put(raw);
				return 4+raw.length;
			}else{
				bb.putInt(0);
				return 4;
			}
		}catch(BufferOverflowException e){
			throw new OverflowException();
		}
	}

	@Override
	public void putByte(byte b) throws OverflowException {
		try{
			bb.put(b);
		}catch(BufferOverflowException e){
			throw new OverflowException();
		}
	}


	@Override
	public int position() {
		return bb.position();
	}

	@Override
	public int position(int p) {
		bb.position(p);
		return bb.position();
	}

	@Override
	public int remaining() {
		return bb.remaining();
	}

	@Override
	public boolean hasInt() {
		return bb.remaining()>=4;
	}
	
	@Override
	public boolean hasShort() {
		return bb.remaining()>=2;
	}

	@Override
	public boolean hasFloat() {
		return bb.remaining()>=4;
	}

	@Override
	public boolean hasString() {
		return bb.remaining()>=4;
	}

	@Override
	public boolean hasByte() {
		return bb.remaining()>=1;
	}


	@Override
	public byte[] getBytes() {
		return bb.array();
	}


	@Override
	public int getNamed(String name) {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public int setNamed(String name) {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public int enterSection() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public int exitSection() {
		// TODO Auto-generated method stub
		return 0;
	}




}
