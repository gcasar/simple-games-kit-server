package si.evil.util;

import java.io.UnsupportedEncodingException;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;

/**
 * Compatible with JavaByteSerializer. Is BIG ENDIAN. Bad representation of floats.
 * @author gcasar
 *
 */
public class GWTByteSerializer implements Serializer {
	public static final float FLOAT_ACCURACY = 10000;
	/**
	 * in bytes
	 */
	public static final int BUFFER_SIZE = 5*1024;
	
	byte[] wrapped = null;
	int position = 0;
	
	protected byte[] buffer = new byte[BUFFER_SIZE];
	
	@Override
	public void wrap(byte[] buffer) {
		position = 0;
		wrapped = buffer;
	}

	@Override
	public void useInternal() {
		wrap(buffer);
	}

	@Override
	public float readFloat() throws UnderflowException {
		int raw = readInt();
		return raw/FLOAT_ACCURACY;
	}

	@Override
	public int readInt() throws UnderflowException {
		try{
			int bytes = 0;
	        for (int i = 0; i < 4; i++) {
	            bytes = bytes << 8;
	            bytes = bytes | (wrapped[position++] & 0xFF);
	        }
	        return bytes;
		}catch(Exception e){
			throw new UnderflowException();
		}
	}

	@Override
	public short readShort() throws UnderflowException {
        try{
    		short bytes  = 0;
            bytes = (short) (wrapped[position++] << 8);
            bytes |= (wrapped[position++] & 0xFF);  
            return bytes;
		}catch(Exception e){
			throw new UnderflowException();
		}
	}

	@Override
	public String readString() throws UnderflowException {
		String result;
		try{
			int len = readInt();
			if(len>0){
				//Try to read the string
				if(len>remaining())throw new UnderflowException();
				try {
					result = new String(wrapped, position, len, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					System.out.println("Serializer: bad encoding.");
					throw new UnderflowException();
				}
				position(position()+len);
				return result;
			}else return "";
		}catch(BufferUnderflowException e){
			throw new UnderflowException();
		}
	}

	@Override
	public byte readByte() throws UnderflowException {
		 try{
    		return wrapped[position++];
		}catch(Exception e){
			throw new UnderflowException();
		}
	}

	@Override
	public void putFloat(float f) throws OverflowException {
		try{
			putInt((int) (f*FLOAT_ACCURACY));
		}catch(BufferOverflowException e){
			throw new OverflowException();
		}
	}

	@Override
	public void putInt(int value) throws OverflowException {
		try{
			for (int i = 3; i >= 0; i--) {
	            wrapped[position + i]= (byte) (value & 0xFF);
	            value = value >> 8;
	        }
			position+=4;
		}catch(Exception e){
			throw new OverflowException();
		}
	}

	@Override
	public void putShort(short value) throws OverflowException {
		try{
			wrapped[position++] =  (byte) ((value >> 8) & 0xFF);
			wrapped[position++] = (byte) (value & 0xFF);
		}catch(Exception e){
			throw new OverflowException();
		}
	}

	@Override
	public int putString(String str) throws OverflowException {
		try{
			byte[] raw = null;
			if(str!=null){
				try {
					raw = str.getBytes("UTF-8");
				} catch (UnsupportedEncodingException e) {
					System.out.println("Serializer: bad encoding (put string).");
					throw new OverflowException();
				}
				putInt(raw.length);
				for(int i=0; i<raw.length; i++)//copy
					putByte(raw[i]);
				return 4+raw.length;
			}else{
				putInt(0);
				return 4;
			}
		}catch(BufferOverflowException e){
			throw new OverflowException();
		}
	}

	@Override
	public void putByte(byte b) throws OverflowException {
		try{
			wrapped[position++] = b;
		}catch(Exception e){
			throw new OverflowException();
		}
	}

	@Override
	public boolean hasInt() {
		return remaining()>=4;
	}

	@Override
	public boolean hasFloat() {
		return remaining()>=4;
	}
	
	@Override
	public boolean hasShort() {
		return remaining()>=2;
	}

	@Override
	public boolean hasString() {
		return remaining()>=4;
	}

	@Override
	public boolean hasByte() {
		return remaining()>=1;
	}

	@Override
	public int position() {
		return position;
	}

	@Override
	public int position(int p) {
		return position = p;
	}

	@Override
	public int remaining() {
		if(wrapped==null)
			return 0;
		else return wrapped.length-position;
	}

	@Override
	public byte[] getBytes() {
		return wrapped;
	}

	@Override
	public int getNamed(String name) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int setNamed(String name) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int enterSection() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int exitSection() {
		// TODO Auto-generated method stub
		return 0;
	}

}
