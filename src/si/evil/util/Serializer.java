package si.evil.util;

public interface Serializer {
	public void wrap(byte[] buffer);
	public void useInternal();
	
	public float readFloat() throws UnderflowException;
	public int readInt() throws UnderflowException;
	public short readShort() throws UnderflowException;
	public String readString() throws UnderflowException;
	public byte readByte() throws UnderflowException;
	
	public void putFloat(float f) throws OverflowException;
	public void putInt(int i) throws OverflowException;
	public void putShort(short i) throws OverflowException;
	/**
	 * 
	 * @param str
	 * @return new position
	 * @throws OverflowException
	 */
	public int putString(String str) throws OverflowException;
	public void putByte(byte b) throws OverflowException;
	
	/**
	 * @param name
	 * @return section or position identifier
	 */
	public int getNamed(String name);
	
	public int setNamed(String name);
	
	/**
	 * Use for reading and writing<br>
	 * id can be position or anything else
	 * @return new section id or -1 if no section
	 */
	public int enterSection();
	/**
	 * Use for reading and writing
	 * @return parent (new section) id
	 */
	public int exitSection();
	
	public boolean hasInt();
	public boolean hasShort();
	public boolean hasFloat();
	public boolean hasString();
	public boolean hasByte();
	
	public int position();
	public int position(int p);
	
	public int remaining();
	
	public byte[] getBytes();
}
