package si.evil.server;

import java.io.UnsupportedEncodingException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Immutable object. <br>
 * Contains Bytes and String representation of data payload
 * it is required that both representations are present, but 
 * it may happen that only one is initialized if UnsupportedEncodingException
 * happens.
 * @author Gregor
 *
 */
public final class Buffer {
	public static Logger log = Logger.getLogger("WServer");
	/**
	 * In Bytes
	 */
	public final int MAX_DATA_SIZE = 1500;
	public final byte [] bytes;
	private String string;
	
	/**
	 * Generates message from data
	 * encoding is ASCII
	 * @param buffer must have appropriate position and limit
	 */
	public Buffer(ByteBuffer buffer){
		int size = buffer.limit()-buffer.position();
		bytes = new byte[size];
		buffer.get(bytes,0,size);
	}
	
	/**
	 * Generates message from bytes<br>
	 * Encoding is ASCII
	 * @param bytes
	 */
	public Buffer(byte[] bytes){
		this.bytes = bytes;
	}
	
	/**
	 * Generates bytebuffer from message
	 * encoding is ASCII
	 * @param message
	 */
	public Buffer(String message){
		this.string = message;
		byte[] tmp;
		try {
			tmp = this.string.getBytes("ASCII");
		} catch (UnsupportedEncodingException e) {
			tmp = new byte[1];
			tmp[0] = 0;
		}
		this.bytes = tmp;
	}
	
	/**
	 * Shallow copy
	 * @param b
	 */
	public Buffer(Buffer b){
		this.bytes = b.bytes;
		this.string = b.string;
	}
	
	/**
	 * Dummy constructor
	 */
	public Buffer(){
		this.bytes = new byte[1];
		this.bytes[0] = 0;
		this.string = "";
	}
	
	public String getString(){
		if(this.string!=null)return string;
		else return string = bytesToAsciiString(this.bytes);
	}
	
	public static String bytesToAsciiString(byte[] bytes){
		String tmp;
		try {
			tmp = new String(bytes,0,bytes.length,"ASCII");
		} catch (UnsupportedEncodingException e) {
			tmp = "";
		}
		return tmp;
	}
}
