package si.evil.simpleg.server;

import java.nio.channels.SocketChannel;


import si.evil.messages.Message;
import si.evil.server.Buffer;
import si.evil.server.Connection;
import si.evil.server.WServer;

public class MConnection extends Connection {
	
	MServer master;
	
	/**
	 * Holds the player that belongs to this connection
	 */
	Player player;
	
	GameServer gs;
	
	int id;

	@Override
	public void onRecv(Buffer data) {
		if(this.gs==null){//handle if dont jet belong to a GameServer
			String msg = data.getString().trim();
			String[] parts = msg.split(" ");
			
			
			if(parts.length>0){
				if(parts[0].compareToIgnoreCase("JOIN")==0){
					player = master.join(this, (parts.length>1)?parts[1]:"");
					if(player!=null){
						gs = player.gameServer;
						this.sendMessage(new Buffer("Failed\n"));
					}
				}else if(parts[0].compareToIgnoreCase("LIST")==0){
					String list = master.list("");
					this.sendMessage(new Buffer(list));
				}else if(parts[0].compareToIgnoreCase("RESTART")==0){
					master.restart();
				}else if(parts[0].compareToIgnoreCase("STOP")==0){
					master.stop();
				}else if(parts[0].compareToIgnoreCase("STATUS")==0){
					String list = master.status("");
					this.sendMessage(new Buffer(list));
				}else{//Log wierd message
					System.out.println("Wierd message: \n"+msg);

					this.sendMessage(new Buffer("Unknown command: "+parts[0].toUpperCase()+'\n'));
				}
			}else{
				//Log wierd message
				System.out.println("Wierd message: \n"+msg);

				this.sendMessage(new Buffer("Unknown command: "+parts[0].toUpperCase()+'\n'));
			}
		}else{
			gs.recvRawMessage(this, data);
			//if(msg!=null){
			//	gs.recvMessage(player, msg);
			//}
		}
	}

	public MConnection(MServer ms, SocketChannel c, WServer server) {
		super(c, server);
		master = ms;
	}

}
