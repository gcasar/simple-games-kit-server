package si.evil.simpleg.server;

import java.util.ArrayList;

import si.evil.messages.Message;
import si.evil.messages.MessageDispatcher;
import si.evil.messages.Target;


/**
 * Manages a simple lobby than spawns a new GameServer when all players are ready
 * TODO
 * @author gcasar
 *
 */
public class LobbyServer extends GameServer {
	
	private ArrayList<Integer> players = new ArrayList<Integer>();
	
	public LobbyServer(MessageDispatcher disp) {
		super(disp);
	}

	@Override
	public boolean recvMessage(Target tid, Message msg) {
		return super.recvMessage(tid, msg);
	}
	
	@Override
	public Player onJoin(Target p) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onDsc(Player p) {
		// TODO Auto-generated method stub
		
	}
	
	
	
}
