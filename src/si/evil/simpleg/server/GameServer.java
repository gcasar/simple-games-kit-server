package si.evil.simpleg.server;

import java.util.ArrayList;
import java.util.HashMap;

import si.evil.messages.DummyTarget;
import si.evil.messages.Message;
import si.evil.messages.MessageDispatcher;
import si.evil.messages.RemoteMessage;
import si.evil.messages.Target;
import si.evil.server.Buffer;
import si.evil.util.GWTByteSerializer;
import si.evil.util.Serializer;

/**
 * Implements puppet message passing
 * @author gcasar
 *
 */
public abstract class GameServer extends DummyTarget {
	
	public static final int MODE_LOOPBACK = 0;
	public static final int MODE_BLUETOOTH = 1;
	public static final int MODE_WEB = 2;
	
	MessageDispatcher dispatcher = null;
	
	GameServerManager gsmanager = null;
	
	HashMap<Integer, Player> players = new HashMap<Integer, Player>();
	
	//protected int mode = MODE_LOOPBACK;


	public GameServer(MessageDispatcher disp) {
		super(DummyTarget.generateNewUID());
		dispatcher = disp;
	}
	
	/**
	 * Call this super to init with starting players (upgrade or switch on gameserver)
	 */
	public GameServer(MessageDispatcher disp, ArrayList<Player> players){
		super(DummyTarget.generateNewUID());
		
	}
	
	/**
	 * Implement this to create your own Player object
	 * @param p
	 * @return
	 */
	public abstract Player onJoin(Target p);
	
	public abstract void onDsc(Player p);

	public boolean recvMessage(Target sender, Message msg) {

		return false;
	}

	/**
	 * Only used in MServer and just because the real dispatch hirearchy is not implemented
	 * @param player
	 * @param data
	 */
	public void recvRawMessage(MConnection player, Buffer data) {
		
	}
}
