package si.evil.simpleg.server;

import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import si.evil.messages.Message;
import si.evil.messages.MessageDispatcher;
import si.evil.messages.Target;
import si.evil.server.Buffer;
import si.evil.server.Connection;
import si.evil.server.WServer;
import si.evil.util.GWTByteSerializer;
import si.evil.util.Serializer;

public class MServer extends MessageDispatcher implements WServer.ConnectionFactory{
	public static void main(String[] args) {		
		//Supply our own factory
		MServer factory = new MServer(args);
	}
	
	public static final String TAG = "MServer 1.0";
	
	WServer server;
	
	
	/**
	 * Default serializer. This is used when a target does not have a GameServer (should not happen?)
	 */
	Serializer serializer;
	
	HashMap<String, GameServerManager> gservers = new HashMap<String, GameServerManager>();
	
	/**
	 * For two way connection to Player<->MConnection (MConnection has its reference to Player)
	 */
	HashMap<Player, MConnection> gplayers = new HashMap<Player, MConnection>();
	
	
	MServer(String[] args){
		//read init file
		
		
		start();
	}




	@Override
	public Connection createNew(SocketChannel c, WServer s) {
		return new MConnection(this,c,s);
	}

/////////////MessageGeneration methods///////////////////////////////////////////////////
	
	/**
	 * Default join. Tries to join a random game. TODO: refactor to specify the game
	 * @param mConnection
	 */
	public Player join(MConnection mConnection, String gameType) {
		GameServer gs = null;
		GameServerManager m = gservers.get(gameType);
		if(m==null)return null;

		Player tmp = new Player();
		gs = m.join(tmp);
		
		if(gs!=null){
			gs.onJoin(tmp);
			return tmp;
		}
		
		return null;
	}


	/**
	 * Lists all games filtered by query
	 * @param query
	 * @return
	 */
	public String list(String query) {
		// TODO Auto-generated method stub
		return null;
	}

/////////////////MessageDispatcher methods////////////////////////////////////////////////
	
	@Override
	public int register(Target t) {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public int uid(Target t) {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public void sendMessage(Target p, Message msg) {
		MConnection con = gplayers.get(p);
		
		Buffer tmp = new Buffer(msg.serialize(serializer));
		
		if(con!=null){
			con.sendMessage(tmp);
		}
	}


	@Override
	public void brodcastMessage(Message msg) {
	}
	
	
	public void multicastMessage(GameServer server, Message msg){
		multicastMessage((Collection)server.players.values(), msg);
	}


	@Override
	public void multicastMessage(Collection<Target> targets, Message msg) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void invMulticastMessage(Collection<Target> ignored, Message msg) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void invSendMessage(Target ignored, Message msg) {
		// TODO Auto-generated method stub
		
	}


	public String status(String string) {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		sb.append(TAG).append('\n');
		sb.append('>').append("Running time ").append("??:??:??").append('\n');
		sb.append('>').append("Players: ").append("???").append('\n');
		sb.append('>').append("Servers: ").append("???").append('\n');
		return sb.toString();
	}
	
	private void start() {
		if(server!=null){
			server.stop();
		}
		server = new WServer(this);
		if(server.init())
			server.serve();
		else
			System.out.println("There was an error while initialising.");
	}



	public void stop() {
		if(server!=null)server.stop();
	}


	public void restart() {
		stop();
		start();
	}
}

