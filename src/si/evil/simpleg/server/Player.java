package si.evil.simpleg.server;

import si.evil.messages.DummyTarget;
import si.evil.messages.Message;
import si.evil.messages.RemoteMessage;
import si.evil.messages.Target;


public class Player implements Target {

	protected static int last_id = 0;
	

	public static final int UNKNOWN_ID = -1;
	
	public static final int COLOR_RED = 0xFF0000FF;

	public static final int COLOR_BLUE = 0x0000FFFF;

	public static final int COLOR_GREEN = 0x00FF00FF;
	
	public static final int COLOR_YELLOW = 0xFFFF00FF;

	public static final int COLOR_SPECTATOR = 0x0;
	
	public String nickname;
	
	public int color;
	
	/**
	 * serves as a Target identifier
	 */
	public int uid;

	GameServer gameServer;
	
	public Player(GameServer gs) {
		gameServer = gs;
	}
	
	Player() {
		gameServer = null;
	}
	
	public Player(Player b) {
		this.uid = uid;
		color = b.color;
		nickname = b.nickname;
		gameServer = b.gameServer;
	}
	
	
	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}
	
	public void setSpectator(boolean spectator){
		if(spectator){
			color = color&0xffFFff00;
		}else{
			color = color|0xFF;
		}
	}
	
	public void setRemoteControl(boolean s){
		if(s){
			color = color&0xffFFffEE;
		}else{
			color = color|0xEE;
		}
	}
	
	public boolean isSpectator(){
		return (color|0xFF)!=0x00;
	}
	
	public boolean isRemoteControl(){
		return (color|0xFF)==0xEE;
	}
	
	@Override
	public boolean recvMessage(Target sender, Message msg) {
		return false;
	}

	@Override
	public int getUID() {
		return uid;
	}
	
	/**
	 * Simple sequential implementation
	 * @param existing
	 * @return
	 */
	public static int generateNewUID(){
		return (++last_id);
	}
	
	
}
