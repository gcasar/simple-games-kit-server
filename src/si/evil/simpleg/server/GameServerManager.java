package si.evil.simpleg.server;

import java.util.Collection;

import si.evil.messages.MessageDispatcher;

/**
 * Abstract GameServer factory
 * @author gcasar
 *
 */
public abstract class GameServerManager {
	
	public abstract GameServer join(Player p);
	
	/**
	 * Called when our dispatcher issues a global tick.<br>
	 * All Managers that spawn GameServers that have steps should issue those steps in this call, 
	 * so the Master server can easily find bottlenecks.
	 * @param dtime
	 */
	public void tick(int dtime){
		//default does nothing
	}
	
	/**
	 * Called when our host is running low on idle proc time<br>
	 * A GameServer manager should stop spawning new servers
	 */
	public void onLowProc(){
		
	}
	
	/**
	 * Called when our host is running low on ram<br>
	 */
	public void onLowRam(){
		
	}
	
	public void onRamRestored(){
		
	}
	
	public void onProcRestored(){
		
	}
	
	public abstract Collection<?> list(String params);

	public abstract void attach(MServer mserver);
}
