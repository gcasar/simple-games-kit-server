package si.evil.simpleg.messages;

import java.io.UnsupportedEncodingException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;

import si.evil.messages.Message;
import si.evil.util.OverflowException;
import si.evil.util.Serializer;
import si.evil.util.UnderflowException;

/**
 * _ indicates that the field should not be changed except if you know what you are doing 
 * Format: (DWORD)[name:String](color:DWORD)[pid:DWORD]
 * Max length (in bytes): 4+100+4+4=112B
 * @author gcasar
 *
 */
public class MsgPlayer extends Message {
	public static final short MSG_ID = 0x01;
	
	public static final short MSG_MAX_LENGTH = 418;
	
	public static final int NAME_MAX_LENGTH = 100;
	
	public static final int UNKNOWN_PID = -1;
	
	public String _name;
	
	public int _color;
	
	public boolean _ready = false;
	
	/**
	 * Player ID. 
	 */
	public int _pid = UNKNOWN_PID;
	
	public void setName(String string) {
		if(string.length()>NAME_MAX_LENGTH)
			_name = string.substring(0, NAME_MAX_LENGTH);
		else _name = string;
	}
	
	@Override
	public boolean parse(Serializer s) {
		try{
			if(s.readShort()!=MSG_ID)return false;
			int length = s.readShort();
			_name = s.readString();
			_color = s.readInt();
			_ready = s.readInt()==0?false:true;
			if(s.hasInt()){
				_pid = s.readInt();
			}else _pid = UNKNOWN_PID;
		}catch(UnderflowException e){
			return false;
		}
		return true;
	}

	
	@Override 
	public int toBytes(Serializer s){
		int start = s.position();
		try {
			s.putShort(MSG_ID);
			int length_pos = s.position();
			s.putShort((short) -1);
			s.putString(_name);
			s.putInt(_color);
			s.putInt(_ready?1:0);
			if(_pid!=UNKNOWN_PID)
				s.putInt(_pid);
			int tmp = s.position();
			s.position(length_pos);
			s.putShort((short) (tmp-start));
			s.position(tmp);
		} catch (OverflowException e) {
			return -(s.position()-start);
		}
		return s.position()-start;
	}



}
