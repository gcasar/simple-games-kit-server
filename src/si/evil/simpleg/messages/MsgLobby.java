package si.evil.simpleg.messages;

import si.evil.messages.BufferedMessage;
import si.evil.util.OverflowException;
import si.evil.util.Serializer;
import si.evil.util.UnderflowException;

public class MsgLobby extends BufferedMessage{
	public static final short MSG_ID = 0x02;

	public static final int ACTION_START = 0;
	
	public static final int ACTION_KICK = 1;

	public static final int ACTION_BAN = 2;
	
	public static final int ACTION_PROMOTE = 3;
	
	
	public int _action;
	
	/**
	 * Player to perform action on
	 */
	public int _pid;

	@Override
	public boolean parse(Serializer s) {
		try{
			if(s.readShort()!=MSG_ID)return false;
			int length = s.readShort();
			_action = s.readInt();
			_pid = s.readInt();
		}catch(UnderflowException e){
			return false;
		}
		return true;
	}
	
	@Override 
	public int toBytes(Serializer s){
		int start = s.position();
		try {
			s.putShort(MSG_ID);
			s.putShort(((short) 12));
			s.putInt(_action);
			s.putInt(_pid);
		} catch (OverflowException e) {
			return -(s.position()-start);
		}
		return s.position()-start;
	}


}
